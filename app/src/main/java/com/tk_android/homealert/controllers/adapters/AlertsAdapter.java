package com.tk_android.homealert.controllers.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tk_android.homealert.R;
import com.tk_android.homealert.models.Movement;
import com.tk_android.homealert.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tk-android on 6/2/17.
 */

public class AlertsAdapter extends RecyclerView.Adapter<AlertsAdapter.AlertViewHolder>{

    private Context context;
    private List<Movement> data = new ArrayList<>();

    public AlertsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public AlertViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_alert, parent, false);
        return new AlertViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlertViewHolder holder, int position) {
        Movement movement = data.get(position);
        if (movement != null){

            Date date = Utils.parseDateFromString(movement.getCreated_at(), context);
            String stringDate = Utils.parseStringFromDate(date, "hh:mm aa d/M/yyyy", context);
            String information = stringDate + " - " + movement.getSensor();

            holder.information.setText(information);
        }
    }

    public void addItems(List<Movement> items){
        data.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class AlertViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.information)
        TextView information;
        public AlertViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
