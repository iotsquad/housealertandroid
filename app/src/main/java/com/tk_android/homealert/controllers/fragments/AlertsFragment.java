package com.tk_android.homealert.controllers.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tk_android.homealert.HouseAlertApplication;
import com.tk_android.homealert.R;
import com.tk_android.homealert.controllers.adapters.AlertsAdapter;
import com.tk_android.homealert.models.Movement;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tk-android on 5/30/17.
 */

public class AlertsFragment extends Fragment {

    @BindView(R.id.rv_alerts)
    RecyclerView rv_alerts;

    AlertsAdapter adapter;

    public static AlertsFragment create(){
        AlertsFragment frag = new AlertsFragment();


        return frag;
    }

    public AlertsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alerts, container, false);
        ButterKnife.bind(this, view);

        rv_alerts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rv_alerts.setHasFixedSize(true);
        adapter = new AlertsAdapter(getContext());
        rv_alerts.setAdapter(adapter);
        loadAlerts();

        return view;
    }

    public void loadAlerts(){
        HouseAlertApplication.getInstance()
                .getServices()
                .getMovements()
                .enqueue(new Callback<List<Movement>>() {
                    @Override
                    public void onResponse(Call<List<Movement>> call, Response<List<Movement>> response) {
                        if (response.isSuccessful()){
                            adapter.addItems(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Movement>> call, Throwable t) {

                    }
                });
    }
}
