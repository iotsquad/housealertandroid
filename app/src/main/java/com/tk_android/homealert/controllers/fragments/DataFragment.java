package com.tk_android.homealert.controllers.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.tk_android.homealert.utils.FirebaseRefs;
import com.tk_android.homealert.R;
import com.tk_android.homealert.models.TestUnit;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tk-android on 5/30/17.
 */

public class DataFragment extends Fragment {

    @BindView(R.id.tv_celsius)
    TextView tv_celsius;
    @BindView(R.id.tv_fht)
    TextView tv_fht;
    @BindView(R.id.tv_humidity)
    TextView tv_humidity;
    @BindView(R.id.chart)
    LineChart line_chart;

    final List<Entry> humidityEntries = new ArrayList<>();
    final List<Entry> tempEntries = new ArrayList<>();

    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
           String key =  dataSnapshot.getRef().getKey();
            if (key.equalsIgnoreCase("today_humidity_percentage")){
                humidityEntries.clear();
                int x = 0;
                for (DataSnapshot snap : dataSnapshot.getChildren()){
                    Integer u = snap.getValue(Integer.class);
                    humidityEntries.add(new Entry(x++, u));
                }
            }else{
                tempEntries.clear();
                int x = 0;
                for (DataSnapshot snap : dataSnapshot.getChildren()){
                    Integer u = snap.getValue(Integer.class);
                    tempEntries.add(new Entry(x++, u));
                }
            }

            LineDataSet dataSet = new LineDataSet(humidityEntries, "Humidity"); // add entries to dataset
//            dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataSet.setDrawFilled(true);
//            dataSet.setFillColor(ContextCompat.getColor(getContext(),R.color.transparentPrimaryDark));
            dataSet.setColor(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));
            dataSet.setCircleColor(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));
            dataSet.setCircleColorHole(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));


            LineDataSet dataTemp = new LineDataSet(tempEntries, "Temperature"); // add entries to dataset
//            dataTemp.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataTemp.setDrawFilled(true);
//            dataTemp.setFillColor(ContextCompat.getColor(getContext(),R.color.transparentPrimary));
            dataTemp.setColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            dataTemp.setCircleColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            dataTemp.setCircleColorHole(ContextCompat.getColor(getContext(),R.color.colorPrimary));
//                dataSet.setValueTextColor(...);

            final List<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.clear();
            if (!tempEntries.isEmpty())dataSets.add(dataTemp);
            if (!humidityEntries.isEmpty())dataSets.add(dataSet);

            LineData lineData = new LineData(dataSets);
            line_chart.setData(lineData);
            line_chart.invalidate();

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public static DataFragment create(){
        DataFragment frag = new DataFragment();
        return frag;
    }

    public DataFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        ButterKnife.bind(this, view);

        line_chart.getAxisRight().setEnabled(false);
        line_chart.getXAxis().setDrawGridLines(false);
        line_chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        FirebaseRefs.getInstance().getDailyTemperature().addValueEventListener(listener);
        FirebaseRefs.getInstance().getDailyHumdity().addValueEventListener(listener);

        FirebaseRefs.getInstance().getTemperature().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tv_celsius.setText(dataSnapshot.getValue(Double.class).toString()+"º C");
                double temps = ((dataSnapshot.getValue(Double.class))* 9/5.0) +32;
                tv_fht.setText(String.format("%.2f", temps)+"º F");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseRefs.getInstance().getHumidity().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tv_humidity.setText(dataSnapshot.getValue().toString()+"%");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }
}
