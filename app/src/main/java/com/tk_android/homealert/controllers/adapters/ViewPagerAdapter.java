package com.tk_android.homealert.controllers.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tk-android on 5/30/17.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter{
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    FragmentManager manager;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
        this.manager = manager;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void addFragment(Fragment fragment, String title, int position){
        mFragmentTitleList.add(position, title);
        mFragmentList.add(position, fragment);
        notifyDataSetChanged();

        FragmentTransaction ft = manager.beginTransaction();
        ft.detach(mFragmentList.get(position));
        ft.attach(mFragmentList.get(position));
        ft.commit();
    }

    public void removeFragment(int position){
        manager.beginTransaction().remove(mFragmentList.get(position)).commit();
        mFragmentList.remove(position);
        mFragmentTitleList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
