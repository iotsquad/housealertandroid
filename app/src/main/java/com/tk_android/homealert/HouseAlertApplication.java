package com.tk_android.homealert;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;
import com.tk_android.homealert.Retrofit.RequestManager;
import com.tk_android.homealert.Retrofit.Services;

/**
 * Created by tk-android on 5/31/17.
 */

public class HouseAlertApplication extends Application {

    private Services services, authServices;
    private static HouseAlertApplication instance;


    public static HouseAlertApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        services = new RequestManager().getWebServices();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
    public Services getServices() {
        return services;
    }

    public Services getAuthServices() {
        return authServices;
    }

    public void setAuthServices() {
        this.authServices = new RequestManager(this).getWebServices();
    }

}
