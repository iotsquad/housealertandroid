package com.tk_android.homealert.models;

/**
 * Created by tk-android on 6/2/17.
 */

public class Movement {

    private int id;
    private String value;
    private String sensor;
    private String created_at;
    private String updated_at;
    private String url;

    public Movement() {
    }

    public Movement(int id, String value, String sensor, String created_at, String updated_at, String url) {
        this.id = id;
        this.value = value;
        this.sensor = sensor;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getSensor() {
        return sensor;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUrl() {
        return url;
    }
}
