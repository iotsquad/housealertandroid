package com.tk_android.homealert.models;

/**
 * Created by tk-android on 5/31/17.
 */

public class TestUnit {
    private int number;

    public TestUnit(int number) {
        this.number = number;
    }

    public TestUnit() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
