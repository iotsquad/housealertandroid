package com.tk_android.homealert.utils;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tk-android on 6/2/17.
 */

public class Utils {

    public static Date parseDateFromString(String stringDate, Context context) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ", context.getResources().getConfiguration().locale);//"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
            return sdf.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String parseStringFromDate(Date stringDate, String format, Context context) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, context.getResources().getConfiguration().locale);
        return sdf.format(stringDate);
    }
}
