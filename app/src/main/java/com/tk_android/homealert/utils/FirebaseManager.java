package com.tk_android.homealert.utils;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by tk-android on 5/30/17.
 */

public class FirebaseManager {

    private static FirebaseDatabase db;


    public static FirebaseDatabase getDatabase(){
        return (db == null)? db = FirebaseDatabase.getInstance(): db;
    }
}
