package com.tk_android.homealert.utils;

import com.google.firebase.database.DatabaseReference;

/**
 * Created by tk-android on 5/31/17.
 */

public class FirebaseRefs {

    private static FirebaseRefs instance;

    public static FirebaseRefs getInstance(){
        return (instance == null)? instance = new FirebaseRefs(): instance;
    }

    public DatabaseReference getDailyTemperature(){
        return  FirebaseManager.getDatabase().getReference().child("today_temperature_celcius");
    }

    public DatabaseReference getDailyHumdity(){
        return  FirebaseManager.getDatabase().getReference().child("today_humidity_percentage");
    }

    public DatabaseReference getHumidity(){
        return  FirebaseManager.getDatabase().getReference().child("last_humidity_percentage");
    }

    public DatabaseReference getTemperature(){
        return  FirebaseManager.getDatabase().getReference().child("last_temperature_celcius");
    }

}
