package com.tk_android.homealert.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by rubymobile on 7/26/16.
 */
public class APIerror {
    @SerializedName("error")
    @Expose
    private Error error;

    public APIerror() {
    }

    public String getMessage() {
        return error.getMessage();
    }

    public HashMap<String, String> getReasons() {
        return error.getReasons();
    }

    public class Error {
        @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("reasons")
        @Expose
        private HashMap<String, String> reasons;

        public Error() {
        }

        public String getMessage() {
            return message;
        }

        public HashMap<String, String> getReasons() {
            return reasons;
        }
    }

}
