package com.tk_android.homealert.Retrofit;

import com.google.gson.JsonObject;
import com.tk_android.homealert.models.Movement;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by rubymobile on 7/26/16.
 */
public interface Services {

    @GET(Urls.MOVEMENTS)
    Call<List<Movement>> getMovements();


/* Test to send image by Retrofit 2.0 */
//    @Multipart
//    @POST(Urls.MESSAGE)
//    Call<JsonObject> sendMessage(@Part("message[sender_id]") RequestBody sender_id , @Part("message[receiver_user_id]") RequestBody receiver_user_id, @Part MultipartBody.Part file, @Part("message[content]") RequestBody content);

}
